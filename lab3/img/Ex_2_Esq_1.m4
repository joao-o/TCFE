% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS                            # Pic input begins with .PS
cct_init                       # Read in macro definitions and set defaults

l = 0.75                       # Variables are allowed; default units are inches
Origin: Here                   # Position names are capitalized
   source(up_ l,S); llabel(,"$v_{i}$",);
   resistor(right_ l); llabel(,"$R$",);
   {
     dot
     diode(down_ l); rlabel(,"$D_{1}$",);
     dot;
   }
   line right 0.5*l;
   {
     dot
     diode(down_ l,,R); rlabel(,"$D_{2}$",);
     {
       move up 0.55*l right 0.07*l
       em_arrows(N,45,0.2*l);
     }
     dot;
   }
   line right 0.4*l;
   gap(down_ l); llabel(+,"$v_0$",-);
   line to Origin
.PE

