% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS                            # Pic input begins with .PS
cct_init                       # Read in macro definitions and set defaults

l = 0.75                       # Variables are allowed; default units are inches
Origin: Here                   # Position names are capitalized
   source(up_ l,S); llabel(,"$v_{i}$",);
   resistor(right_ l); llabel(,"$R$",);
   dot;
   diode(down_ l); rlabel(,"$D_{1}$",);
   dot;
   {
        line right 0.4*l;
        gap(up_ l);
        rlabel(-,"$v_0$",+);
        dot;
        line left 0.4*l;
        dot;
   }
   line left 0.5*l;
   ground;
   line to Origin
.PE

