
% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS                            # Pic input begins with .PS
cct_init                       # Read in macro definitions and set defaults

l = 0.75                       # Variables are allowed; default units are inches
Origin: Here                   # Position names are capitalized
   battery(up_ l*1.5); llabel(,"$V_{cc}$",);
   resistor(right_ l); llabel(,"$R_1$",);
   {
     dot
     resistor(down_ l*0.8); rlabel(,"$R_2$",);
     source(down_ 0.7*l,S); rlabel(,"$v_i$",)
     dot
   }
   line right l*0.8
   {
     {
       move left 0.5*l
       dot
       line down 0.75*l
     }
     move down 0.75*l left 0*l
     mosfet(up_ l*1.5,,MEDSuBQ);
   }
   {
     move right 0.2*l down 0.25*l
     arc -> cw to Here.x`,'Here.y-1*l from Here rad 1*l
     move up 0.5*l right 0.35*l
     "$v_{GS}$"  
   }
   move down l*1.5
   line left l*1.8
   dot
   ground
.PE
