
% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS                            # Pic input begins with .PS
cct_init                       # Read in macro definitions and set defaults

l = 0.75                       # Variables are allowed; default units are inches
Origin: Here                   # Position names are capitalized
   battery(up_ l*1); llabel(,"$V_{cc}$",);
   resistor(right_ l); llabel(,"$R_1$",);
   line right 0.3*l
   {
     {
       move left 0.5*l
       dot
       line down 0.5*l
     }
     move down 0.5*l left 0*l
     mosfet(up_ l,,MEDSuBQ);
   }
   {
     move right 0.2*l
     arc -> cw to Here.x`,'Here.y-1*l from Here rad 1*l
     move up 0.5*l right 0.35*l
     "$v_{GS}$"  
   }
   move down l*1
   line to Origin
   dot
   ground
.PE
