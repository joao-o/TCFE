% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS                            # Pic input begins with .PS
cct_init                       # Read in macro definitions and set defaults

l = 0.75                       # Variables are allowed; default units are inches
Origin: Here                   # Position names are capitalized
   source(up_ l,S); llabel(,"$v_{i}$",);
   resistor(right_ l); llabel(,"$R$",);
   dot;
   line right 0.4*l;
   gap(down_ l);
   llabel(+,"$v_0$",-);
   dot;
   line left 0.4*l;
   dot;
   {
        diode(up_ l,);
        llabel(,"$D_{2}$",);
        {
          move down 0.33 right 0.07
          em_arrows(N,45,0.2*l);
        }
   }
   line left 0.5*l;
   ground;
   line to Origin
.PE
