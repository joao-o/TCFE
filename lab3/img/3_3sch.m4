

% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS                            # Pic input begins with .PS
cct_init                       # Read in macro definitions and set defaults

l = 0.75                       # Variables are allowed; default units are inches
Origin: Here                   # Position names are capitalized
   resistor(up_ l*1.5); llabel(,"$R_1$",);
   line right 0.5*l
   {
     dot
     resistor(down_ l*0.8); rlabel(,"$R_2$",);
     source(down_ 0.7*l,S); rlabel(,"$v_i$",)
     dot
   }
   line right 0.5*l
   resistor(down_ l*1.5); rlabel(,"$R_3$",)
   line to Origin
   dot
   ground
.PE
