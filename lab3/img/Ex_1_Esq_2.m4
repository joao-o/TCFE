% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS                            # Pic input begins with .PS
cct_init                       # Read in macro definitions and set defaults

l = 0.75                       # Variables are allowed; default units are inches
Origin: Here                   # Position names are capitalized
   source(up_ l,V); llabel(,"$V_{g}$",);
   gap(right_ l);
   {
   diode(left_ l); rlabel(,"$D$",);
   }
   dot;
   {
	line right 0.4*l;
	dot;
	gap(down_ l);
	llabel(+,"$v_0$",-);
	dot;	
	line left 0.4*l;

   }
   resistor(down_ l); rlabel(,"$R$",);
   dot;
   line to Origin
   ground(,)
.PE
