reset

# epslatex
set terminal epslatex size 9cm, 7cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output '1wave.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#1b6ddc' lt 1 lw 2
set style line 2 lc rgb '#1bdc1f' lt 1 lw 2
set style line 3 lc rgb '#dc261b' lt 1 lw 2
set key box

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 11 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1.5
set grid xtics ytics ls 12

set xrange [0:0.001]
#set yrange [0:10]

#axis labels
set xlabel '$t(s)$'
set ylabel '$v_i,v_o (V)$' offset 1.5

f=1000;
Vg=0.6;
Vm=2.5;
pi=3.14159265358;
v1=0.6;
t1=asin(Vg/2.5)*1/(2*pi*f);

Vi(x)=Vm*sin(2*pi*f*x)
Vo(x)=(x>t1 && x<(0.0005-t1))? (Vi(x)-Vg) : 0
set samples 10000
plot Vi(x) title '$v_i$' ls 1, \
     Vo(x) title '$v_o$' ls 2
