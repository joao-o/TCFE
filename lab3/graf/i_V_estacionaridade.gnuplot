reset

# epslatex
set terminal epslatex size 18cm, 14cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'iVestacionaridade.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#1b6ddc' lt 1 lw 2 ps 3
set style line 2 lc rgb '#1bdc1f' lt 1 lw 7 ps 3 pt 6

set style line 3 lc rgb '#800000' lt 1 lw 1.5 pt 2 ps 1.5 
set style line 4 lc rgb '#800000' lt 1 lw 1.5 pt 5 ps 1.5
set style line 5 lc rgb '#800000' lt 1 lw 1.5 pt 1 ps 2
set style line 6 dt 8 lc rgb'#404040' lt 1 lw 1 pt 7 ps 1.5  
unset key

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1.5
set grid mxtics ytics xtics ls 12

set xrange [0:2.3]
#set yrange [-22:2]


#axis labels
set xlabel '$V_{GS}~(V)$'
set ylabel '$i_D~(A)$'

set tics scale 2

#set logscale x 10

pi=3.14159265358;
k=0.030;
Vt=1.9;




i(x)=(x>Vt)? k*(x-Vt)*(x-Vt): 0
set samples 10000

set arrow from 1.95486,0 to 1.95486,i(1.95486) nohead ls 6
set arrow from 2.13395,0 to 2.13395,i(2.13395) nohead ls 6
set arrow from 2.24231,0 to 2.24231,i(2.24231) nohead ls 6
set arrow from 0,i(1.95486) to 1.95486,i(1.95486) nohead ls 6
set arrow from 0,i(2.13395) to 2.13395,i(2.13395) nohead ls 6
set arrow from 0,i(2.24231) to 2.24231,i(2.24231) nohead ls 6


plot i(x) notitle ls 1,\
    'estacionaridade.txt' u 1:(i($1)) title "Dados" ls 2

