reset

# epslatex
set terminal epslatex size 18cm, 14cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output '4ganh.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#000000' lt 1 lw 2
set style line 2 lc rgb '#d05000' lt 1 lw 2

set style line 3 lc rgb '#800000' lt 1 lw 1.5 pt 2 ps 1.5
set style line 4 lc rgb '#800000' lt 1 lw 1.5 pt 1 ps 1.5
set style line 5 lc rgb '#800000' lt 1 lw 1.5 pt 1 ps 2
unset key

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1.5
set grid mxtics ytics xtics ls 12

set xrange [10:100000]
set yrange [-22:2]


#axis labels
set xlabel '$f(Hz)$'
set ylabel '$\left|\frac{V_{s}}{v_{G}}\right|_{dB}$'

set tics scale 2

set logscale x 10

ganho(x)=20*(log10(r2/(r1+r2))+\
             0.5*(log10(1+(2*pi*x/w1)**2)-\
                  log10(1+(2*pi*x/w2)**2)))

pi=3.14159265358;

r1=2700e0;
r2=330e0;
c1=220e-9;
c2=220e-123;
w1=1/(r1*c1);
w2=(r1+r2)/((c1+c2)*r1*r2);

plot ganho(x) notitle ls 1 ,\
     '4ganh.txt' u 1:2:3:4 title "Dados" w boxxyerrorbars ls 4,\
     '4ganh.txt' u 1:2 title "Dados" ls 5
