reset

# epslatex
set terminal epslatex size 18cm, 14cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'iV.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#1b6ddc' lt 1 lw 2
set style line 2 lc rgb '#1bdc1f' lt 1 lw 2

set style line 3 lc rgb '#800000' lt 1 lw 1.5 pt 2 ps 1.5
set style line 4 lc rgb '#800000' lt 1 lw 1.5 pt 1 ps 1.5
set style line 5 lc rgb '#800000' lt 1 lw 1.5 pt 1 ps 2
unset key

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1.5
set grid mxtics ytics xtics ls 12

set xrange [0:2.3]
#set yrange [-22:2]


#axis labels
set xlabel '$V_{GS}~(V)$'
set ylabel '$i_D,~\sqrt{i_D}~(A)$'

set tics scale 2

#set logscale x 10

pi=3.14159265358;
k=0.030;
Vt=1.9;


i(x)=(x>Vt)? k*(x-Vt)*(x-Vt): 0
sqrti(x)=(x>Vt)?sqrt(k*(x-Vt)*(x-Vt)):0
set samples 10000



plot i(x) notitle ls 1,\
     sqrti(x) notitle ls 2


#     '4ganh.txt' u 1:2:3:4 title "Dados" w boxxyerrorbars ls 4,\
