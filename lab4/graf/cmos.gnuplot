reset

# epslatex
set terminal epslatex size 9cm, 7cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'cmos.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#1b6ddc' lt 1 lw 2
set style line 2 lc rgb '#1bdc1f' lt 1 lw 2
set style line 3 lc rgb '#dc261b' lt 1 lw 2
set style line 4 lc rgb '#000000' lt 1 lw 1

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1.5
set grid xtics ytics ls 12

set xrange [0:12]
set yrange [-0.5:13]

#axis labels
set xlabel '$v_i (V)$'
set ylabel '$v_o (V)$' offset 1.5

#arrow
set arrow from 1.9,-0.5 to 1.9,13 nohead ls 4
set arrow from 10.6,-0.5 to 10.6,13 nohead ls 4
set arrow from 6.25,-0.5 to 6.25,13 nohead ls 4
set arrow from 0,4.35 to 12,4.35 nohead ls 4
set arrow from 0,7.65 to 12,7.65 nohead ls 4

set label "I" at 1,10
set label "II" at 4,10
set label "III" at 5,6
set label "IV" at 8,2
set label "V" at 11,2

set label "(10.6,0)" at 8.5,1
set label "(6.25,4.35)" at 6.5,5
set label "(6.25,7.65)" at 6.5,8.2
set label "(1.9,12)" at 2.2,12.5

vdd=12.0
rd1=2200.0
a=2*6.5e-4
vtn=1.9
vtp=1.4

Vo2k(x)=vdd*(x<vtn)+(x+vtp+sqrt((vdd-x-vtp)**2-(x-vtn)**2))*(x>vtn && x<(vdd-vtp+vtn)/2)+\
(x-vtn-sqrt(-(vdd-x-vtp)**2+(x-vtn)**2))*(x<vdd-vtp && x>(vdd-vtp+vtn)/2)

plot Vo2k(x) notitle ls 1,\
     "pts.txt" u 1:2 notitle
