reset

# epslatex
set terminal epslatex size 9cm, 7cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output '1wave.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#1b6ddc' lt 1 lw 2
set style line 2 lc rgb '#1bdc1f' lt 1 lw 2
set style line 3 lc rgb '#dc261b' lt 1 lw 2
set key box

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 11 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1.5
set grid xtics ytics ls 12

set xrange [0:12]
set yrange [0:13]

#axis labels
set xlabel '$v_i$'
set ylabel '$v_o$' offset 1.5

vdd=12.0
rd1=2200.0
a=2*6.5e-4
vt=1.9
v1=4.49

Vo2k(x)=vdd*(x<vt)+(vdd-rd1*a/2*(x-vt)**2)*((x>vt)&&(x<v1))+\
(x-vt+1/rd1/a-sqrt((x-vt+1/rd1/a)**2-2*vdd/a/rd1))*(x>v1)

rd2=100000.0
v2=2.32
Vo100k(x)=vdd*(x<vt)+(vdd-rd2*a/2*(x-vt)**2)*((x>vt)&&(x<v2))+\
(x-vt+1/rd2/a-sqrt((x-vt+1/rd2/a)**2-2*vdd/a/rd2))*(x>v2)

plot Vo2k(x) title '$ R_D=2.2k\Omega$' ls 1,\
     Vo100k(x) title '$ R_D=100k\Omega$' ls 2 
