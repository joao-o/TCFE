% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS 2.6 
cct_init                       # Read in macro definitions and set defaults

Origin: Here                   # Position names are capitalized
  dot; llabel(,"$S$",);
  move up 1
  dot; llabel(,"$G$",);
  line right 1
  dot
  move right 1
  {
  dot
  consource(down_ 1,I); ; llabel(,"$g_mv_{GS}$",);
  dot
  }
  line right 1
  dot; llabel(,"$D$",);
  move down 1
  dot
  line to Origin

.PE
