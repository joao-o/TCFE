
% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS 3 
cct_init                       # Read in macro definitions and set defaults

Origin: Here                   # Position names are capitalized
source(up_ 1,S);larrow("$v_i$",->);
resistor(right_ 0.3,0);b_current("$i_1$",,,);
{
  dot
  resistor(down_ 1);llabel(,"$R_1//R_2$",);
  dot
}
line right 0.9
{
  move right 0.1
  "G"
}
gap(down_ 0.6)
{
  resistor(down_ 0.4);llabel(,"$R_s$",)
  dot
}
{
  line left 0.1
  lswitch(left_ 0.2,R,C);
  dot
  line left 0.05
  line down 0.4
  dot
}
line right 0.6
{
line right 0.15
dot
move right 0.1
"S"
}

consource(up_ 0.6,I,R);llabel(,"$g_mv_{gs}$",)
dot
line right 0.15
dot
{
  move up .1
  "D"
}
resistor(right_ 0.35,0);b_current("$i_{d0}$",,O,E);
{
  dot;
  resistor(down_ 1);llabel(,"$R_D$",);b_current("$i_{d}$",,,)
  dot
}
line right 0.4
{
  dot;
  resistor(down_ 1);llabel(,"$R_L$",);b_current("$i_{0}$",,,)
  dot
}
line right 0.3
gap(down_ 1);larrow("$v_0$",->);
line to Origin
ground
.PE
