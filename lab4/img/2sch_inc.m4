% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS 2.6 
cct_init                       # Read in macro definitions and set defaults

Origin: Here                   # Position names are capitalized
   ground
   source(up_ 1); llabel(,"$v_{i}$",);
   line right 0.5
   {
    dot
    resistor(down_ 1);llabel(,"$R_1||R_2$",)
   {
     dot
     resistor(right_ 0.65,0); dot; move down 0.05; A3:Here; b_current("$i_G\approx 0$",,,E)
   }
   {
   move left 0.05 down 0.05
   B1: Here
   }
     resistor(down_ 0.6);llabel(,"$R_1$",)
   {
   move left 0.05 up 0.05
   B2: Here
   }
     dot
   }
   resistor(right_ 1,0); b_current("$I_D$",,,)
   resistor(down_ 0.5);rlabel(,"$R_D$",);
   {
     move right 0.05 down 0.05
     A1: Here
   }
   {
     move left 0.1875
     down
     mosfet(up_ 0.4,,MEDSuBQ);
     move right 0.1875
     {
      move right 0.05 up 0.05
      A2: Here
     }
     {
      move left 0.05
      A4: Here
     }
     dot
     resistor(down_ 0.4);rlabel(,"$R_S$",)
     dot
   }
   dot
   line right 0.5
   gap(down_ 0.8);larrow("$v_o$",->);
   line to Origin
   arc  -> cw from A1 to A2  
   move up 0.19 right 0.2
   "$v_{DS}$"
   arc  -> ccw from A3 to A4
   move down 0.07 left 0.3
   "$v_{GS}$" 
   arc  <- cw from B2 to B1 rad 1
   move down 0.25 left 0.2
   "$v_{R1}$"
.PE
