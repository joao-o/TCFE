
% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS 2.6 
cct_init                       # Read in macro definitions and set defaults

Origin: Here                   # Position names are capitalized
   battery(up_ 1.5); llabel(,"$V_{DD}$",);
   line right 0.8 
   resistor(down_ 0.6);rlabel(,"$R_D$",)
   {
    dot
    line right 0.2
    gap(down_ 0.9);larrow("$v_o$");
   }
   move left 0.1875
   down
   mosfet(up_ 0.5,,MEDSuBQ);
   move right 0.1875
   line down 0.4
   { 
     dot
     line right 0.2
   }
   line left 0.37
   {
     source(up_ 0.65,V,0.2); llabel(,"$v_i$",)
   }
   line to Origin
   ground

   move right 1.6 
     
   Origin2: Here

   battery(up_ 1.5); llabel(,"$V_{DD}$",);
   line right 0.8 
   move left 0.1875
   down
   mosfet(down_ 0.6,R,MEDSdBQ,);
   move right 0.1875
   {
    dot
    line right 0.2
    gap(down_ 0.9);larrow("$v_o$");
   }
   move left 0.1875
   down
   mosfet(up_ 0.5,,MEDSuBQ);
   move right 0.1875
   line down 0.4
   { 
     dot
     line right 0.2
   }
   line left 0.37
   {
     source(up_ 0.65,V,0.2); llabel(,"$v_i$",)
     line up 0.55
   }
   line to Origin2
   ground
.PE
