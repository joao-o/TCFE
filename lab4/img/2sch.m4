
% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS 2.6 
cct_init                       # Read in macro definitions and set defaults

Origin: Here                   # Position names are capitalized
   battery(up_ 1.3); llabel(,"$V_{DD}$",);
   line right 1.0
   {
    dot
    resistor(down_ 0.7);rlabel(,"$R_2$",)
     {
       {line right 0.2}
       dot
       capacitor(left_ 0.5); rlabel(,"$C$",);b_current("$i_i$",below_,,E)
       {
         gap(down_ 0.6);larrow("$r_i$",<-);
       }
       line left 0.15
       source(down_ 0.6,S,0.2);rlabel(,"$v_i$",) 
       dot
     }
     resistor(down_ 0.6);rlabel(,"$R_1$",)
     dot
   }
   line right 0.5
   resistor(down_ 0.5);rlabel(,"$R_D$",)
   {
     move right 0.05 down 0.05
     A1: Here
   }
   {
     move left 0.1875
     down
     mosfet(up_ 0.4,,MEDSuBQ);
     move right 0.1875
     {
      move right 0.05 up 0.05
      A2: Here
     }
     {
       dot
       line right 0.15
       lswitch(right_ 0.1,,C);llabel(,,"$S$");
       move right 0.02
       line right 0.10
       capacitor(down_ 0.4);llabel(,"$C_s$",)
       dot
     }
     resistor(down_ 0.4);rlabel(,"$R_S$",)
     dot
   }
   dot
   capacitor(right_ 0.7); llabel(,"$C$",);
   {
     dot
     resistor(down_ 0.8);llabel(,"$R_L$",);
     dot
   }
   line right 0.3
   gap(down_ 0.8);larrow("$v_o$",->);
   line to Origin
   ground
   arc  -> cw from A1 to A2   
   move up 0.19 right 0.2
   "$v_{DS}$"
.PE
