% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS                            # Pic input begins with .PS
cct_init                       # Read in macro definitions and set defaults

l = 0.75                    # Variables are allowed; default units are inches
Origin: Here                   # Position names are capitalized
   line up l
   line right 0.4*l
   {
     gap(down_ l)
   }
   {
     resistor(down_ l);llabel(,R,);
   }
   resistor(right_ l);llabel(,R,);
   {
     resistor(down_ l);rlabel(,R,);
   }
   dot;
   resistor(right_ l);llabel(,R,);
   CT:dot
   gap(down_ l);llabel($2$,,"\linhaa")
   resistor(left_ l);rlabel(,R,);
   dot
   {
   resistor(from Here to CT);move left 0.6*l down 0.3*l;"R";
   }
   resistor(left_ l);rlabel(,R,);
   line to Origin
.PE
