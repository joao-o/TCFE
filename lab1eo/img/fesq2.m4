% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS                            # Pic input begins with .PS
cct_init                       # Read in macro definitions and set defaults

l = 0.75                    # Variables are allowed; default units are inches
Origin: Here # Position names are capitalized
   {	
   resistor(up_ l);llabel(,2R,);
   }
   line right 0.4*l
   {
     resistor(up_ l);rlabel(,R,);
     line left 0.4*l
   }
  gap(up_ l)
  resistor(right_ l);rlabel(,R,);
  CT:dot
  gap(down_ l);llabel($2$,,"\linhaa")
  resistor(left_ l);rlabel(,R,);
  resistor(from Here to CT);move left 0.6*l down 0.32*l;"R";
.PE
