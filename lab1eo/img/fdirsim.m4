
% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS                            # Pic input begins with .PS
cct_init                       # Read in macro definitions and set defaults

l = .75                    # Variables are allowed; default units are inches5
Origin: Here                   # Position names are capitalized
   dot
   line left 0.4*l
   gap(up_ l);llabel("\linhab",,1)
   line right 0.4*l
   dot
   {
     resistor(down_ l);llabel(,R,);
   }
   resistor(right_ l);llabel(,R,);
   {
     resistor(down_ l);rlabel(,R,);
   }
   dot;
   resistor(right_ l);llabel(,R,);
   CT:dot
  
   line down l 
   resistor(left_ l);rlabel(,R,);
   dot
   {
   resistor(from Here to CT);move left 0.4*l down 0.6*l;"R";
   }
   resistor(left_ l);rlabel(,R,);
   line to Origin
   { 
     move left l*0.6 up l*0.5
     arc <- cw from Here.x-0.4*l`,'Here.y-1.5*l to Here  rad 2
   }
   move down l*1.5 left 0.4*l

   dot
   line left 0.4*l
   gap(up_ l);llabel("\linhab",,1)
   line right 0.4*l
   dot
   {
     resistor(down_ l);llabel(,R,);
   }
   resistor(right_ l);llabel(,R,);
   {
     resistor(down_ l);rlabel(,R,);
   }
   dot
   line right l*0.3 
   resistor(down_ l);llabel(,"$\frac{3}{2}R$",);
   line left l*0.3 
   dot
   resistor(left_ l);rlabel(,"R",);

   move right 2.4*l
   { 
     move left l*0.45 up l*1.1
     arc -> cw from Here.x-0.6*l`,'Here.y to Here  rad 1
   } 
   dot
   line left 0.4*l
   gap(up_ l);llabel("\linhab",,1)
   line right 0.4*l
   dot
   {
     resistor(down_ l);rlabel(,R,);
   }
   line right l*0.3 
   resistor(down_ l);llabel(,"$\frac{13}{5}R$",);
   line left l*0.3 


.PE
