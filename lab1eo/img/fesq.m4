% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS                            # Pic input begins with .PS
cct_init                       # Read in macro definitions and set defaults

l = 1                    # Variables are allowed; default units are inches
Origin: Here                   # Position names are capitalized
   battery(up_ l,); llabel(,"$\epsilon$",);
   {
     arc -> ccw to Here.x+0.2*l`,'Here.y-0.6*l from Here.x+0.4*l`,'Here.y-0.65*l with .c at Here.x+0.3*l`,'Here.y-0.5*l "$\gamma_1$"
   }
   line right 0.6*l
   {
     gap(down_ l)
   }
   {
     resistor(down_ l);llabel(,R,);
   }
   {
   arc -> ccw to Here.x+0.3*l`,'Here.y-0.6*l from Here.x+0.65*l`,'Here.y-0.65*l with .c at Here.x+0.5*l`,'Here.y-0.5*l "$\gamma_2$"

   }
   resistor(right_ l);llabel(,R,);
   {
     resistor(down_ l);rlabel(,R,);
   }
   dot;
   {
     arc -> ccw to Here.x+0.3*l`,'Here.y-0.5*l from Here.x+0.8*l`,'Here.y-0.4*l with .c at Here.x+0.4*l`,'Here.y-0.3*l "$\gamma_3$"
   }
   resistor(right_ l);llabel(,R,);
   CT:dot
   gap(down_ l);llabel($2$,,"\linhaa")
   resistor(left_ l);rlabel(,R,);
   dot
   {
   resistor(from Here to CT);move left 0.4*l down 0.6*l;"R";
   }
   resistor(left_ l);rlabel(,R,);
   line to Origin
.PE
