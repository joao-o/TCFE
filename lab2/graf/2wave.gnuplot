reset

# epslatex
set terminal epslatex size 9cm, 7cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output '2wave.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#1b6ddc' lt 1 lw 2
set style line 2 lc rgb '#1bdc1f' lt 1 lw 2
set style line 3 lc rgb '#dc261b' lt 1 lw 2
set key box

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 11 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1.5
set grid xtics ytics y2tics ls 12

set xrange [0:2]
set yrange [0:10]

#axis labels
set xlabel '$t(ms)$'
set ylabel '$V_g,V_s (V)$' offset 1.5
set y2label '$I (mA)$' offset -2.5

set y2tics 0.1 rotate by 90 center


vc(x)=Vcc*(1-exp(-x/(R*C)/1000))*(x<1)+Vcc*exp(-(x-1)/(R*C)/1000)*(x>1)
vg(x)=Vcc*(x<1)
i(x)=(Vcc*(exp(-x/(R*C)/1000))/R*1000)*(x<1)+\
     (-Vcc/R*exp(-(x-1)/(R*C)/1000)*1000)*(x>1)

f=.5e0;
Vcc=8;
pi=3.14159265358;
R=47000e0;
C=2.2e-9;
w1=1/(R*C);

set link y2 via (y-4)/R*2000 inverse y*R/2000+4

plot vc(x) title '$V_c$' ls 1, \
     vg(x) title '$V_g$' ls 2, \
     i(x) axes x1y2 title '$I$' ls 3
