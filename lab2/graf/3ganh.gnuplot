reset

# epslatex
set terminal epslatex size 18cm, 14cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output '3ganh.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#000f0f' lt 1 lw 2

set style line 3 lc rgb '#800000' lt 1 lw 1.5 pt 2 ps 1.5
set style line 4 lc rgb '#000f0f' lt 1 lw 1.5 pt 1 ps 3

unset key

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 3 back ls 11
set tics nomirror out scale 0.2
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1.5
set grid mxtics ytics xtics ls 12

set xrange [10:100000]
set yrange [-40:5]

#axis labels
set xlabel '$f(Hz)$'
set ylabel '$\left|\frac{V_{s}}{v_{G}}\right|_{dB}$'

set tics scale 2

set logscale x 10

ganho(x)=-10*log10(1+(2*pi*x/w1)**2)

pi=3.14159265358;
R=47000;
C=2.2e-9;
w1=1/(R*C);

plot ganho(x) notitle ls 1,\
     '3ganh.txt' u 1:2:3:4 title "Dados" w xyerrorbars ls 3
