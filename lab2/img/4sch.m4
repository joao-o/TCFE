% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS                            # Pic input begins with .PS
cct_init                       # Read in macro definitions and set defaults

l = 0.75                    # Variables are allowed; default units are inches
Origin: Here                   # Position names are capitalized
   source(up_ l,S); llabel(,"$V_{g}$",);
   line right 0.2*l
   dot
   {
     line down 0.25*l
     resistor(right_ l*0.8); llabel(,"$R_1$",);
     line up 0.25*l
   }
   line up 0.25*l
   capacitor(right_ l*0.8); llabel(,"$C_1$",);
   line down 0.25*l
   dot
   line right 0.4*l
   {
   dot
   line down 0.1*l
   {
     line left 0.25*l
     capacitor(down_ l*0.8); rlabel(,"$C_2$",);
     line right 0.25*l
   }
   {
     dot
     line right 0.25*l
     resistor(down_ l*0.8); rlabel(,"$R_2$",);
     line left 0.25*l
     dot
     line down 0.1*l
     dot
   }
   }
   line right 0.5*l
   gap(down_ l); llabel(+,"$v_s$",-);
   line to Origin
   dot
   ground(,)
.PE
