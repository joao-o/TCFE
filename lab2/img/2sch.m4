% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS                            # Pic input begins with .PS
cct_init                       # Read in macro definitions and set defaults

l = 0.75                    # Variables are allowed; default units are inches
Origin: Here                   # Position names are capitalized
   source(up_ l,V); llabel(,"$V_{g}$",);
   resistor(right_ l); llabel(,"$R$",);
   {
     capacitor(down_ l); rlabel(,"$C$",);
   }
   line right 0.2*l
   gap(down_ l); llabel(+,"$v_s$",-);
   line to Origin
   ground(,)
.PE
