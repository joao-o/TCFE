reset

# epslatex
set terminal epslatex size 9cm, 7cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output '2wave.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#1b6ddc' lt 1 lw 2
set style line 2 lc rgb '#1bdc1f' lt 1 lw 2
set style line 3 lc rgb '#dc261b' lt 1 lw 2
set key box nobox bottom left

# Axes
set style line 11 lc rgb '#000000' lt 1
set border 11 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1.5
set grid xtics ytics ls 12

set xrange [-10:10]
#set yrange [0:10]

#axis labels
set xlabel '$v_I~(V)$'
set ylabel '$v_O=v_{CE}~(V)$'
va(x)=14.1-3*x
Vi(x)=((-10<x)&&(x<0))? 12 : ((x>0) && (x<4.467))? va(x):(x>4.467&&x<10)? 0:1/0/0
set samples 10000
plot Vi(x) title "$v_O(v_I)$" ls 1

