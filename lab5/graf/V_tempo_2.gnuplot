reset

# epslatex
set terminal epslatex size 9cm, 7cm color colortext input header \
"\\newcommand{\\ft}[0]{\\footnotesize}\\graphicspath{{./graf/}}"
set output 'V_tempo_2.tex'

# color definitions
set border linewidth 1.5
set style line 1 lc rgb '#1b6ddc' lt 1 lw 2
set style line 2 lc rgb '#1bdc1f' lt 1 lw 2
set style line 3 lc rgb '#dc261b' lt 1 lw 2
#set key box
set key bottom left Right title 'Legenda' nobox
# Axes
set style line 11 lc rgb '#000000' lt 1
set border 11 back ls 11
set tics nomirror out scale 0.5
# Grid
set style line 12 dt 3 lc rgb'#808080' lt 0 lw 1.5
set grid xtics ytics ls 12

set xrange [0:0.01]
#set yrange [0:10]

#axis labels
set xlabel '$t~(s)$'
set ylabel '$v_{i}, v_{CE}~(V)$'
T=1e-2;
A=10;

Vo(x)=((0<x)&&(x<T/4))? 4*A/T*x: ((x>T/4)&&(x<3*T/4))? -4*A/T*x+2*A : ((x>3*T/4)&&(x<T))? 4*A/T*x-4*A:1/0 
va(x)=14.1-3*x
Vi(x)=((-10<x)&&(x<0))? 12 : ((x>0) && (x<4.467))? va(x):(x>4.467&&x<10)? 0:1/0/0
Vd(x)=Vi(Vo(x))
set samples 10000
plot Vo(x) title '$v_I$' ls 1, \
     Vd(x) title '$v_{CE}$'ls 2

