
% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS  
cct_init                       # Read in macro definitions and set defaults

Origin: Here                   # Position names are capitalized
   battery(up_ 1.3); llabel(,"$V_{CC}$",);
   line right 1
   resistor(down_ 0.6);rlabel(,"$R$",);b_current("$i_C$",,O,E);
   dot
   {
     move right 0.05 down 0.05
     A1: Here
   }
   {
     Q1:bi_tr(up_ 0.4) with .C at Here
     resistor(left_ 0.4 from Q1.B);rlabel(,"$R_b$",);b_current("$i_B$",above,O,,3);
     source(down_ 0.5,"$v_i$",0.25);rlabel(+,,-)
     dot
     move to Q1.E
     {
       move right 0.05 up 0.05
       A2: Here
     }
     resistor(down_ 0.3,0);b_current("$i_E$");
     dot
   }
   line right 0.6
   gap(down 0.7); larrow("$v_{o}$");
   line to Origin
   ground
   arc -> cw from A1 to A2 
   move up 0.19 right 0.2
   "$v_{CE}$"
   move to Q1.B down 0.05
   ST: Here
   move to Q1.E left 0.07
   FN: Here
   arc <- cw to ST from FN
   move down 0.15 left 0.03 
   "$v_{BE}$"

.PE
