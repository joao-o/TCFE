% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS 2.8 
cct_init                       # Read in macro definitions and set defaults

Origin: Here                   # Position names are capitalized
source(up_ 1,S);larrow("$v_i$",->);
resistor(right_ 0.3,0);b_current("$i_i$",,,);
{
  dot
  resistor(down_ 1);llabel(,"$R_1//R_2$",);
  dot
}
line right 0.9
{
  resistor(down_ 0.6);rlabel(,"$r_\pi$",);b_current("$i_B$",,,);
}
dot
{
  move  up 0.1
  "B"
}
move down 0.6
{
  move right 0.2
  resistor(down_ 0.4);rlabel(,"$R_E$",)
  dot
}
{ 
  move right 0.5
  dot
  line down 0.1
  lswitch(down_ 0.2,,C)

  line down 0.1
  dot
}
line right 0.7
{
line right 0.15
dot
move down 0.1
"E"
}
dot
consource(up_ 0.6,I,R);llabel(,"$g_mR_\pi$",);rlabel(,"$\beta_fi_B$",)

line right 0.15
dot
{
  move up .1
  "C"
}
resistor(right_ 0.6,0);b_current("$i_{c}$",,O,E);
{
  dot;
  resistor(down_ 1);llabel(,"$R_C$",);
  dot
}
line right 0.3
gap(down_ 1);larrow("$v_0$",->);
line to Origin
ground
.PE
