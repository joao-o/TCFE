
% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS  
cct_init                       # Read in macro definitions and set defaults

Origin: Here                   # Position names are capitalized
   battery(up_ 1.6); llabel(,"$V_{CC}$",);
   line right 0.8
   {
    dot
    resistor(down_ 0.8);rlabel(,"$R_1$",)
     {
       MD:Here
       dot
       capacitor(left_ 0.5); rlabel(,"$C_c$",);b_current("$i_i$",below_,,E)
       source(down_ 0.8,S,0.2);rlabel(,"$v_i$",) 
       dot
     }
     resistor(down_ 0.8);rlabel(,"$R_2$",)
     dot
   }
   line right 0.5
   resistor(down_ 0.6);rlabel(,"$R_C$",);b_current("$i_C$",,O,E);
   dot
   {
     move right 0.05 down 0.05
     A1: Here
   }
   {
     Q1:bi_tr(up_ 0.4) with .C at Here
     resistor(to Q1.B from MD,0);b_current("$i_B$",above,,,1.5)
     move to Q1.E
     {
       move right 0.05 up 0.05
       A2: Here
     }
     {
       dot
       line right 0.15
       switch(right_ 0.1,,C);
       move right 0.02
       line right 0.10
       capacitor(down_ 0.6);rlabel(,"$C_e$",)
       dot
     }
     resistor(down_ 0.6);rlabel(,"$R_E$",);b_current("$i_E$");
     dot
   }
   line right 0.6
   gap(down 1); larrow("$v_{o}$");
   line to Origin
   ground
   arc -> cw from A1 to A2 
   move up 0.19 right 0.2
   "$v_{CE}$"
   move to Q1.B down 0.05
   ST: Here
   move to Q1.E left 0.07
   FN: Here
   arc <- cw to ST from FN
   move down 0.15 left 0.03 
   "$v_{BE}$"

.PE
