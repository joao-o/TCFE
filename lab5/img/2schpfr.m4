
% quick.m4
% https://ece.uwaterloo.ca/~aplevich/Circuit_macros/
.PS  
cct_init                       # Read in macro definitions and set defaults

Origin: Here                   # Position names are capitalized
   battery(up_ 1.6); llabel(,"$V_{CC}$",);
   line right 0.8
   line right 0.5
   resistor(down_ 0.6);rlabel(,"$R_C$",);b_current("$i_C$",,O,E);
   dot
   {
     move right 0.05 down 0.05
     A1: Here
   }
   {
     Q1:bi_tr(up_ 0.4) with .C at Here
     resistor(from Q1.B left_ 0.5);rlabel(,"$R_{TH}$",);b_current("$i_B$",above,,E,4)
     battery(down_ 0.8,,R); llabel(,"$V_{TH}$",);
     dot
     move to Q1.E
     {
       move right 0.05 up 0.05
       A2: Here
     }
     resistor(down_ 0.6);rlabel(,"$R_E$",);b_current("$i_E$");
     dot
     line to Origin
     dot
     ground
   }
   arc -> cw from A1 to A2 
   move up 0.19 right 0.2
   "$v_{CE}$"
   move to Q1.B down 0.05
   ST: Here
   move to Q1.E left 0.07
   FN: Here
   arc <- cw to ST from FN
   move down 0.15 left 0.03 
   "$v_{BE}$"

.PE
